# Otevřená komunikce

# Jak spusit?
```bash
pip install -r requirements.txt
SECRET_KEY=XXXXX flask run
```
V prolížeči otevřít [127.0.0.1:5000](http://127.0.0.1:5000/)


# Dev notes
- Není využito více endpointů pro zpracování formulářů, jelikož by pak nešlo automaticky generovat formuláře.
- Cílem bylo udělat algoritmus a nikoliv produkční aplikaci, tedy devops je potřeba dotánout v případě nasazení.
- ENV proměnné neumí načíst pythonanywhere  