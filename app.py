from os import environ

from flask import Flask, render_template, flash, redirect, url_for, request
from flask_bootstrap import Bootstrap5
from flask_wtf import FlaskForm
from wtforms.fields import PasswordField, SubmitField, StringField
from wtforms.validators import DataRequired

app = Flask(__name__)

#secret_key = environ.get('SECRET_KEY', False)  # na webu nefunguji .env
#if not secret_key:
#    raise RuntimeError('ENV SECRET_KEY must be configured!')
#app.secret_key = secret_key
app.secret_key = 'SETUPoNsERVER'

bootstrap = Bootstrap5(app)


class PublicKeyForm(FlaskForm):
    secret = PasswordField('Moje tajné číslo', validators=[DataRequired()])
    submitPublic = SubmitField('Generovat veřejný klíč')


class PrivateKeyForm(FlaskForm):
    secret = PasswordField('Moje tajné číslo', validators=[DataRequired()])
    remote_public_key = StringField('Veřejný klíč co jsem dostal', validators=[DataRequired()])
    submitPrivate = SubmitField('Generovat soukromý klíč')


@app.route('/', methods=['GET', 'POST'])
def index():
    g = 5
    p = 23

    public_key_form = PublicKeyForm()
    private_key_form = PrivateKeyForm()

    print(public_key_form.submitPublic)
    print(public_key_form)
    if public_key_form.submitPublic.data and public_key_form.validate_on_submit():
        public_key = str((pow(g, int(request.form['secret']))) % p)
        flash('Tento veřejný klíč pošli druhé straně zabezpečeným kanálem: ' + public_key, 'info')
        return redirect(url_for('index'))
    elif private_key_form.submitPrivate.data and private_key_form.validate_on_submit():
        final_key = str((pow(int(request.form['remote_public_key']), int(request.form['secret']))) % p)
        flash('Gratuluji! Váš domluvený společný klíč je: ' + final_key, 'success')
        return redirect(url_for('index'))

    return render_template(
        'index.html',
        public_key_form=public_key_form,
        private_key_form=private_key_form
    )


if __name__ == '__main__':
    app.run(debug=True)
